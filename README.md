# Weather Pipeline
This project experiments with the free [OpenWeather API](#https://openweathermap.org/api) how to query
the current weather data for (longitude, latitude) coordinates across the world. Additionally, the following concepts
are explored:
- Using the **requests** library
- DataFrames and GeoDataFrames from **pandas** and **geopandas**
- postgreSQL database connections with **sqlalchemy**
- GitLab CI/CD for automated and scheduled scripting
  - Dependency Management
  - Secret handling via **GitLab CI/CD Variables** and **python-dotenv**


## Getting started
First, clone the repository to your local storage.  
Next, navigate to the project directory and install the needed libraries found in `requirements.txt`.

## Handling Secrets  
First, create a free account with OpenWeather [here](#https://home.openweathermap.org/users/sign_up). Once the account is created, they will send an API Key over to your email. However, it may take a few hours before the API Key is activated, so be patient if it does not work off the bat. The Free account offers several useful APIs that can be used. In particular, this project will focus on the [Current Weather API](#https://openweathermap.org/current). However, there are plenty of free and paid APIs which you can view [here](https://openweathermap.org/price).  


Next, create or find your [postgreSQL](#https://www.postgresql.org/) database and get the credentials needed to connect. This typically is the following:
- **hostname**: the IP address or hostname of the database
- **port**: the available ports for the database. By default, this is 5432
- **database_name**: the name of the database. By default, this is called "postgres"
- **username**: the username of the user
- **password**: the password of the user


Now that we have identified our secrets, let's set up the environment file to store them. We will use the **python-dotenv** library to handle our secrets and avoid exposing them. This is performed by storing our secrets in a file named `.env` and preventing the file from being uploaded onto version control sites like GitLab via the `.gitignore` file. Within the code, **python-dotenv** will parse through the `.env` and save the contents as environmental variables for use. As a result, the sensitive information is never exposed within the code. However, be sure to avoid mistakenly printing out the secrets!

Now at the top level of the project, create a file named `.env`. Next, edit the file with the following text:
```
WEATHER_API_KEY={secret here}
POSTGRES_HOSTNAME={secret here}
POSTGRES_USER={secret here}
POSTGRES_PASS={secret here}
```
Replace `{secret here}` on each line with your actual sensitive information. Once this is finished, the secret handling is finished on your local machine. You can run `weather.py` and it will execute successfully. However, there's still one more step to handle the secrets on GitLab.

Navigate to the project's GitLab page and go to `Settings > CI/CD > Variables`. Expand the menu and add each of the previously mentioned variables above. By doing this, GitLab will make the variables available to the code. However, `weather.py` is set up to read from a `.env` file. Since we store the `.env` file locally and never version control it, this will cause problems in the CI/CD pipeline. To address this, `setup_env.sh` will use the GitLab variables and create a `.env` file from scratch without ever exposing the sensitive information.

At this stage, we have finished the prep work needed to edit the project. Head over to weather.py and modify it to your liking.

## Using GitLab CI/CD Pipelines for Scheduling
The `.gitlab-ci.yml` file contains information to automatically set up the CI/CD pipeline. Whenever a commit is pushed,
GitLab will automatically create a VM, install needed dependencies or read them from a cache, then run the `weather.py` script. Additionally, you can schedule the pipeline to run at set times by navigating to `Build > Pipeline schedues` on the project's GitLab page.
For free accounts, GitLab provides 400 minutes of usage each month for free. On average, this project takes 59 seconds to build the VM and 1 second to execute for a total runtime of 1 minute. So, setting a schedule for the script to run every 2 hours would keep you under the limit.