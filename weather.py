import os
from datetime import datetime, timezone

import geopandas as gpd
import pandas as pd
import requests
from dotenv import find_dotenv, load_dotenv
from sqlalchemy import create_engine, types

# Start benchmarking the script runtime
start_time = datetime.now()


# Initialize variables
load_dotenv(find_dotenv())
WEATHER_API_KEY = os.getenv("WEATHER_API_KEY")
POSTGRES_HOSTNAME = os.getenv("POSTGRES_HOSTNAME")
POSTGRES_USER = os.getenv("POSTGRES_USER")
POSTGRES_PASS = os.getenv("POSTGRES_PASS")


def query_weather_data(lat: float, lon: float, units="imperial") -> gpd.GeoDataFrame:
    """
    Given a lat/lon coordinate, returns a GeoDataFrame with the current weather information for the area.
    Weather API Guide: https://openweathermap.org/current
    """
    query = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units={units}&appid={WEATHER_API_KEY}"
    res = requests.get(query).json()

    # Parse relevant data from the response
    mapping = {
        "lon": res["coord"]["lon"],
        "lat": res["coord"]["lat"],
        # Convert epoch time to UTC timestamp
        "measured_at": datetime.fromtimestamp(res["dt"], tz=timezone.utc),
        "weather_id": res["weather"][0]["id"],
        "weather_main": res["weather"][0]["main"],
        "weather_description": res["weather"][0]["description"],
        "temperature": res["main"]["temp"],
        "feels_like": res["main"]["feels_like"],
        "humidity": res["main"]["humidity"],
        "temp_min": res["main"]["temp_min"],
        "temp_max": res["main"]["temp_max"],
        "visibility": res["visibility"],
        "wind_speed": res["wind"]["speed"],
        "wind_deg": res["wind"]["deg"],
        "cloudiness": res["clouds"]["all"],
        "sunrise_at": datetime.fromtimestamp(res["sys"]["sunrise"], tz=timezone.utc),
        "sunset_at": datetime.fromtimestamp(res["sys"]["sunset"], tz=timezone.utc),
    }
    # Wrap each value in an array for conversion into a DataFrame. Technically could've hardcoded this above.
    mapping = {k: [v] for (k, v) in mapping.items()}
    df = pd.DataFrame.from_dict(mapping)
    df = gpd.GeoDataFrame(
        df, geometry=gpd.points_from_xy(df["lon"], df["lat"]), crs="EPSG:4326"
    )
    return df


# Query and aggregate data
coordinates = [
    # White House
    ["38.898313634686396", "-77.03656921229923"],
    # Times Square
    ["40.75797093991548", "-73.98554180478516"],
    # Disneyland Park (California)
    ["33.81209776634494", "-117.91896519421863"],
    # NASA Johnson Space Center
    ["29.559342282447545", "-95.08999848755194"],
]
df = None
for [lat, lon] in coordinates:
    if df is None:
        df = query_weather_data(lat, lon)
    else:
        df = pd.concat([df, query_weather_data(lat, lon)])
print(df)

# Upload the data to postgres
connection_string = (
    f"postgresql://{POSTGRES_USER}:{POSTGRES_PASS}@{POSTGRES_HOSTNAME}:5432/postgres"
)
engine = create_engine(connection_string)
dtypes = {
    "lon": types.Numeric,
    "lat": types.Numeric,
    "measured_at": types.TIMESTAMP(timezone=True),
    "weather_id": types.Integer,
    "weather_main": types.Text,
    "weather_description": types.Text,
    "temperature": types.Numeric,
    "feels_like": types.Numeric,
    "humidity": types.Numeric,
    "temp_min": types.Numeric,
    "temp_max": types.Numeric,
    "visibility": types.Integer,
    "wind_speed": types.Numeric,
    "wind_deg": types.Numeric,
    "cloudiness": types.Integer,
    "sunrise_at": types.TIMESTAMP(timezone=True),
    "sunset_at": types.TIMESTAMP(timezone=True),
}
# Upload the data to our existing table on posgreSQL
try:
    df.to_postgis("weather", engine, if_exists="append", index=False, dtype=dtypes)
except:
    print("postgres upload error- most likely a Primary Key issue")

# Finish the benchmarking
end_time = datetime.now()
elapsed_time = end_time - start_time
print(f"Script runtime: {elapsed_time}")
